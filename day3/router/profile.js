const router = require('express').Router()
const profileController = require('../controllers/profileControllers')

router.get('/', profileController.home)

router.get('/all', profileController.all)

router.get('/find/:number', profileController.find)

// teknik url parameter
router.get('/artikel/:id?', profileController.artikel)
//tidak bisa dipanggil di browser karena methodnya post
router.post('/news', profileController.news)

module.exports = router;