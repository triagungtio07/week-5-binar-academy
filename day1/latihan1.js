// fs kepanjangan dari file system
// secara default bisa digunakan di node js
const fs = require('fs')

// fs.rename('before.json', 'after.json', err => {
//   if (err) {
//     return console.error(err)
//   }

//   //done
// })


const handleRenameError = (err) => {
    if (err){
        console.error(err);
    }
    else{
        return console.log('success rename file')
    }
}

fs.rename('after.json', 'before.json', handleRenameError)




