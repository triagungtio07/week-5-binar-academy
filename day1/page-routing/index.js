const express = require('express');
var path = require('path')
const app = express();

app.get("/", (req,res)=>{
    res.sendFile("html/home.html", {root:__dirname} );
});

app.get("/html/invesment.html", (req,res)=>{
    res.sendFile("html/invesment.html", {root:__dirname} );
});

app.get("/html/tech.html", (req,res)=>{
    res.sendFile("html/tech.html", {root:__dirname} );
});

app.get("/html/lifestyle.html", (req,res)=>{
    res.sendFile("html/lifestyle.html", {root:__dirname} );
});

app.get("/html/bisnis.html", (req,res)=>{
    res.sendFile("html/bisnis.html", {root:__dirname} );
});

app.get("/html/sport.html", (req,res)=>{
    res.sendFile("html/sport.html", {root:__dirname} );
});

app.get("/html/trending.html", (req,res)=>{
    res.sendFile("html/trending.html", {root:__dirname} );
});

app.get("/html/international.html", (req,res)=>{
    
    res.sendFile("html/international.html", {root:__dirname} );
});



app.get("/html/national.html", (req,res)=>{
    res.sendFile("html/national.html", {root:__dirname} );
});

app.get("/html/region.html", (req,res)=>{
    res.sendFile("html/region.html", {root:__dirname} );
});


app.listen(5000, () => console.log('app running on port 5000'))