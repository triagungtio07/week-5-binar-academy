const router = require('express').Router();
const dataController = require('../controller/dataController')



router.post('/createUser', dataController.createUser)
router.get('/readUser', dataController.readUser)
router.put('/updateUser', dataController.updateUser)
router.delete('/deleteUser', dataController.deleteUser)

router.post('/createTask', dataController.createTask)
router.get('/readTask', dataController.readTask)
router.put('/updateTask', dataController.updateTask)
router.delete('/deleteTask', dataController.deleteTask)

module.exports= router