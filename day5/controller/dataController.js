class DataController{
    createUser(req,res){
        res.send("Buat tabel user")
    }
    readUser(req,res){
        res.send("Baca tabel user")
    }
    updateUser(req,res){
        res.send("Ubah tabel user")
    }
    deleteUser(req,res){
        res.send("Hapus tabel user")
    }
    createTask(req,res){
        res.send("Buat tabel task")
    }
    readTask(req,res){
        res.send("Baca tabel task")
    }
    updateTask(req,res){
        res.send("Ubah tabel task")
    }
    deleteTask(req,res){
        res.send("Hapus tabel task")
    }
}

module.exports = Object.freeze(new DataController())
