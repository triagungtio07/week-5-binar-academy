const express = require('express')
const app = express()

const dataRouter = require("./router/data")

app.route("/data", dataRouter)

app.get('/user', function (req, res) {
  res.send('Baca Tabel User')
})

app.post('/user', function (req, res) {
  res.send('Buat Tabel User')                  
})

app.put('/user', function (req, res) {
  res.send('Ubah Tabel User')
})


app.delete('/user', function (req, res) {
  res.send('Hapus Tabel User')
})


app.get('/task', function (req, res) {
  res.send('Baca Tabel Task')
})

app.post('/task', function (req, res) {
  res.send('Buat Tabel Task')                  
})

app.put('/task', function (req, res) {
  res.send('Ubah Tabel Task')
})

app.delete('/task', function (req, res) {
  res.send('Hapus Tabel Task')
})


app.listen(3000, ()=> console.log("App running on port 3000"))